package com.dating.customer.advert;

// An object holding information on an advertiser's target match
public class PartnerDescription {
	
	private Range age;
	private Range income;
	private String gender;
	
	public PartnerDescription(String gender, int minAge, int maxAge, int minIncome, int maxIncome) {
		this.age = new Range(minAge, maxAge);
		this.income = new Range(minIncome, maxIncome);
		this.gender = gender;
	}
	
	public boolean isTargetAge(int targetAge) {
		return age.isValid(targetAge);
	}
	
	public boolean hasTargetIncome(int targetIncome) {
		return income.isValid(targetIncome);
	}
	
	public boolean isTargetGender(String targetGender) {
		return gender.equalsIgnoreCase(targetGender);
	}
	
	public String getGender() {
		return gender;
	}
	
	public int getMinAge() {
		return age.getMin();
	}
	
	public int getMaxAge() {
		return age.getMax();
	}
	
	public int getMinIncome() {
		return income.getMin();
	}
	
	public int getMaxIncome() {
		return income.getMax();
	}
	
	private class Range {
		
		private int min;
		private int max;
		
		private Range(int min, int max) {
			this.min = min;
			this.max = max;
		}
		
		private boolean isValid(int i) {
			return (i >= min && i <= max);
		}
		
		private int getMin() {
			return min;
		}
		
		private int getMax() {
			return max;
		}
	}
}