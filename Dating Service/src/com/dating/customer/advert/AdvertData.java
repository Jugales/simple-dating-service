package com.dating.customer.advert;

// Data needed for each advertisement, required by documentation
public class AdvertData {

	private String text;
	
	private PartnerDescription targetPartner;
	
	public AdvertData(String text, PartnerDescription targetPartner) {
		this.text = text;
		this.targetPartner = targetPartner;
	}
	
	public String getText() {
		return text;
	}
	
	public PartnerDescription getTargetPartner() {
		return targetPartner;
	}
}