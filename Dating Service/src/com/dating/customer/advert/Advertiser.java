package com.dating.customer.advert;

import java.util.ArrayList;

import com.dating.customer.Customer;
import com.dating.customer.LoginData;
import com.dating.customer.PersonalDetails;

// The representation of an advertiser and its data
public class Advertiser extends Customer {

	private AdvertData advertData;
	
	private ArrayList<String> messages;
	
	public Advertiser(AdvertData advertData, LoginData loginData, PersonalDetails details) {
		super(loginData, details);
		this.advertData = advertData;
		this.messages = new ArrayList<String>();
		System.out.println("Added " + toString());
	}
	
	public AdvertData getAdvertData() {
		return advertData;
	}
	
	public void addMessage(String message) {
		messages.add(message);
	}
	
	public boolean hasMessages() {
		return !messages.isEmpty();
	}
	
	public ArrayList<String> getMessages() {
		return messages;
	}
	
	@Override
	public String toString() {
		return "advertisor -- username=" + getLoginData().getUsername() + ", password=" + getLoginData().getPassword()
				+ ", age=" + getDetails().getAge() + ", gender=" + getDetails().getGender() + ", income=$" + getDetails().getIncome()
				+ ", advert text=" + '"' + getAdvertData().getText() + '"'
				+ ", target income=$" + getAdvertData().getTargetPartner().getMinIncome() + "-$" + getAdvertData().getTargetPartner().getMaxIncome()
				+ ", target age=" + getAdvertData().getTargetPartner().getMinAge() + "-" + getAdvertData().getTargetPartner().getMaxAge()
				+ ", target gender=" + getAdvertData().getTargetPartner().getGender();
	}
}