package com.dating.customer.respond;

import com.dating.customer.Customer;
import com.dating.customer.LoginData;
import com.dating.customer.PersonalDetails;

// A very simple responder object, not much was needed but the toString() method is handy for logging
public class Responder extends Customer {

	public Responder(LoginData loginData, PersonalDetails details) {
		super(loginData, details);
		System.out.println("Added " + toString());
	}

	@Override
	public String toString() {
		return "responder -- username=" + getLoginData().getUsername() + ", password=" + getLoginData().getPassword()
				+ ", age=" + getDetails().getAge() + ", gender=" + getDetails().getGender() + ", income=$" + getDetails().getIncome();
	}
}