package com.dating.customer.respond;

import java.util.Random;

// An object for choosing a random reply to advertisers. 
public class ResponderMessage {
	
	private String[] messages = new String[] { "I would like to get to know you",
			"You seem pretty cool", "We should get dinner sometime", "Aw, you're cute", 
			"If I knew any cheesy pickup lines, I'd tell you them", "We should date", 
			"Would you like to get married?"};
	
	private Random random;
	
	public ResponderMessage() {
		this.random = new Random();
	}
	
	public String getRandomMessage() {
		return messages[random.nextInt(6)];
	}
}
