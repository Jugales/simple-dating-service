package com.dating.customer;

// A simple customer object holding data that all customers need
public class Customer {
	
	private LoginData loginData;
	private PersonalDetails details;
	
	public Customer(LoginData loginData, PersonalDetails details) {
		this.loginData = loginData;
		this.details = details;
	}
	
	public LoginData getLoginData() {
		return loginData;
	}
	
	public PersonalDetails getDetails() {
		return details;
	}
}