package com.dating.customer;

import com.dating.customer.advert.Advertiser;
import com.dating.customer.advert.PartnerDescription;
import com.dating.customer.respond.Responder;
import com.dating.customer.respond.ResponderMessage;

// Checks for compatibility between advertisers and responders
public class MatchFinder {
	
	private CustomerHandler list;
	
	public MatchFinder(CustomerHandler list) {
		this.list = list;
	}
	
	public void showNewMessages() {
		ResponderMessage message = new ResponderMessage(); // ' Can be replaced later with a up
		for(Responder responder : list.getResponderList()) {
			String gender = responder.getDetails().getGender();
			int age = responder.getDetails().getAge();
			int income = responder.getDetails().getIncome();
			
			for(Advertiser advertiser : list.getAdvertiserList()) {
				PartnerDescription targetPartner = advertiser.getAdvertData().getTargetPartner();
				if(targetPartner.isTargetGender(gender) && targetPartner.isTargetAge(age) && targetPartner.hasTargetIncome(income)) {
					System.out.println(advertiser.getLoginData().getUsername() + " has a new message from " + responder.getLoginData().getUsername());
					advertiser.addMessage(responder.getLoginData().getUsername() + ": " + message.getRandomMessage());
				}
			}
		}
	}
	
	public CustomerHandler getList() {
		return list;
	}
}