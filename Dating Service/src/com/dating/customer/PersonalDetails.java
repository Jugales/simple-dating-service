package com.dating.customer;

// a simple customer details object that every customer needs
public class PersonalDetails {

	private String gender;
	private int age;
	private int income;
	
	public PersonalDetails(String gender, int age, int income) {
		this.gender = gender;
		this.age = age;
		this.income = income;
	}
	
	public String getGender() {
		return gender;
	}
	
	public int getAge() {
		return age;
	}
	
	public int getIncome() {
		return income;
	}
}