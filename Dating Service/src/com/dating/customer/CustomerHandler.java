package com.dating.customer;

import java.util.ArrayList;

import com.dating.customer.advert.AdvertData;
import com.dating.customer.advert.Advertiser;
import com.dating.customer.advert.PartnerDescription;
import com.dating.customer.respond.Responder;

// handles adding, removing, and editing of customer lists
public class CustomerHandler {

	private ArrayList<Advertiser> advertisers;
	private ArrayList<Responder> responders;
	
	public CustomerHandler() {
		this.advertisers = new ArrayList<Advertiser>();
		this.responders = new ArrayList<Responder>();
		addAll(); // ' Programmatically enters all users
	}
	
	public void addAdvert(Advertiser advert) {
		advertisers.add(advert);
	}
	
	public void addResponder(Responder responder) {
		responders.add(responder);
	}
	
	public ArrayList<Advertiser> getAdvertiserList() {
		return advertisers;
	}
	
	public ArrayList<Responder> getResponderList() {
		return responders;
	}
	
	public void checkAdvertAccounts() {
		// ' "Sign in" to all advertising accounts and check for messages
		System.out.println("CHECKING ADVERTISEMENT ACCOUNTS:");
		for(Advertiser advertiser: advertisers) {
			System.out.println("Signing into " + advertiser.getLoginData().getUsername() + " . . . ");
			if(advertiser.hasMessages()) {
				for(String message : advertiser.getMessages()) {
					System.out.println("Message from " + message);
				}
			} else {
				System.out.println("No messages to show");
			}
		}
	}
	
	public void removeAccount(String username) {
		boolean found = false;
		for(Advertiser advertiser : advertisers) {
			if(advertiser.getLoginData().getUsername().equalsIgnoreCase(username)) {
				advertisers.remove(advertiser);
				found = true;
			}
		}
		for(Responder responder : responders) {
			if(responder.getLoginData().getUsername().equalsIgnoreCase(username)) {
				responders.remove(responder);
				found = true;
			}
		}
		
		if(!found)
			System.out.println("Username not found");
		else
			System.out.println("Removed user: " + username);
	}
	
	public void removeAllAccounts() {
		System.out.println("REMOVING ALL ACCOUNTS: ");
		advertisers.clear();
		responders.clear();
		System.out.println("All customers removed");
	}
	
	public void logUsernames() {
		for(Advertiser advertiser : advertisers) {
			System.out.println(advertiser.getLoginData().getUsername());
		}
		for(Responder responder : responders) {
			System.out.println(responder.getLoginData().getUsername());
		}
	}
	
	private void addAll() {
		// ' Add 7 advertisers
		System.out.println("ADDING 7 ADVERTISERS:");
		advertisers.add(new Advertiser(new AdvertData("I'm looking for a girlfriend", new PartnerDescription("female", 25, 30, 20000, 80000)), new LoginData("Jeremy748", "dfhsiuhsf"), new PersonalDetails("male", 27, 30000)));
		advertisers.add(new Advertiser(new AdvertData("I've been feeling lonely lately", new PartnerDescription("female", 35, 45, 30000, 100000)), new LoginData("JamesSwendol", "ilivelong"), new PersonalDetails("male", 38, 52000)));
		advertisers.add(new Advertiser(new AdvertData("I've been searching for someone", new PartnerDescription("male", 32, 34, 15600, 73000)), new LoginData("JessicaB", "puppypower"), new PersonalDetails("female", 31, 30000)));
		advertisers.add(new Advertiser(new AdvertData("I want someone I can be happy with", new PartnerDescription("female", 18, 25, 13000, 63000)), new LoginData("Dude123", "threeeight9"), new PersonalDetails("male", 21, 24000)));
		advertisers.add(new Advertiser(new AdvertData("Is there anyone interesting out there?", new PartnerDescription("male", 21, 23, 25400, 76400)), new LoginData("CaitlynWow", "this237stuff"), new PersonalDetails("female", 23, 28300)));
		advertisers.add(new Advertiser(new AdvertData("I'm ready to settle down", new PartnerDescription("male", 25, 30, 32400, 66900)), new LoginData("JensenHelo", "10001110101"), new PersonalDetails("female", 41, 68700)));
		advertisers.add(new Advertiser(new AdvertData("I'm looking for a girlfriend", new PartnerDescription("female", 20, 28, 21200, 94600)), new LoginData("Robbie_theMan", "youknowit"), new PersonalDetails("male", 24, 42400)));
		
		// ' Add 7 responders
		System.out.println("ADDING 7 RESPONDERS:");
		responders.add(new Responder(new LoginData("ChloeSav", "cheetoh"), new PersonalDetails("female", 26, 34000)));
		responders.add(new Responder(new LoginData("BenJames", "434yeah"), new PersonalDetails("male", 32, 22000)));
		responders.add(new Responder(new LoginData("Survivor911", "funinthesun"), new PersonalDetails("female", 21, 40000)));
		responders.add(new Responder(new LoginData("ThomasCoach", "omwout"), new PersonalDetails("male", 27, 18000)));
		responders.add(new Responder(new LoginData("KimAnne", "heretoparty"), new PersonalDetails("female", 43, 24000)));
		responders.add(new Responder(new LoginData("LinsdayDunn", "itsunreal"), new PersonalDetails("female", 25, 27000)));
		responders.add(new Responder(new LoginData("PhillipT", "thecoolest"), new PersonalDetails("male", 24, 68000)));
	}
}
