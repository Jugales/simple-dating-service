package com.dating.io;

import com.dating.customer.Customer;

public interface CustomerRegistration {

	public Customer getCustomer();
}
