package com.dating.io;

import java.util.Scanner;

import com.dating.customer.Customer;
import com.dating.customer.LoginData;
import com.dating.customer.PersonalDetails;
import com.dating.customer.advert.AdvertData;
import com.dating.customer.advert.Advertiser;
import com.dating.customer.advert.PartnerDescription;

public class AdvertiserRegistration implements CustomerRegistration {

	private Advertiser advertiser;
	// Responsible for the registration form of an advertiser
	public AdvertiserRegistration() {
		@SuppressWarnings("resource") // resource not closed here, but it is closed in Application.java
		Scanner io = new Scanner(System.in);
		
		String username;
		String password;
		String gender;
		int age;
		int income;
		String advertText;
		String targetGender;
		int minAge;
		int maxAge;
		int minIncome;
		int maxIncome;

		
		boolean invalid;
		do {
			invalid = false;
			try {
				System.out.println("Fill out the following information to add an advertiser.");
				System.out.println("Username: "); username = io.nextLine();
				System.out.println("Password: "); password = io.nextLine();
				System.out.println("Gender: "); gender = io.nextLine();
				boolean validGender = checkGender(gender);
				if(!validGender) {
					invalid = true;
					break;
				}
				System.out.println("Age: "); age = io.nextInt();
				System.out.println("Income: "); income = io.nextInt();
				io.nextLine(); // nextInt() does not skip to a new line
				System.out.println("Please state your advertisement text and partner preferences below.");
				System.out.println("Advertisement Text: "); advertText = io.nextLine();
				System.out.println("Gender: "); targetGender = io.next();
				boolean validTargetGender = checkGender(targetGender);
				if(!validTargetGender) {
					invalid = true;
					break;
				}
				System.out.println("Minimum Age: "); minAge = io.nextInt();
				System.out.println("Maximum Age: "); maxAge = io.nextInt();
				System.out.println("Minimum Income: "); minIncome = io.nextInt();
				System.out.println("Maximum Income: "); maxIncome = io.nextInt();
				
				advertiser = new Advertiser(new AdvertData(advertText, new PartnerDescription(targetGender, minAge, maxAge, minIncome, maxIncome)),
											new LoginData(username, password), 
											new PersonalDetails(gender, age, income));
			} catch(Exception e) {
				System.out.println("Invalid input detected. Please start over.");
				invalid = true;
			}
		} while(invalid);
		//io.close();
	}
	
	private boolean checkGender(String gender) {
		if(gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("female"))
			return true;
		System.out.println("Gender must be stated as 'male' or 'female'");
		return false;
	}
	
	@Override
	public Customer getCustomer() {
		return advertiser;
	}
}
