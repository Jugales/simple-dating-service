package com.dating.io;

import java.util.Scanner;

import com.dating.customer.Customer;
import com.dating.customer.LoginData;
import com.dating.customer.PersonalDetails;
import com.dating.customer.respond.Responder;

public class ResponderRegistration implements CustomerRegistration {

	private Responder responder;
	// Responsible for the registration form of a responder
	public ResponderRegistration() {
		@SuppressWarnings("resource") // resource not closed here, but it is closed in Application.java
		Scanner io = new Scanner(System.in);
		
		String username;
		String password;
		String gender;
		int age;
		int income;

		boolean invalid;
		do {
			invalid = false;
			try {
				System.out.println("Fill out the following information to add an responder.");
				System.out.println("Username: "); username = io.nextLine();
				System.out.println("Password: "); password = io.nextLine();
				System.out.println("Gender: "); gender = io.nextLine();
				
				boolean validGender = checkGender(gender);
				if(!validGender) {
					invalid = true;
					break;
				}
				
				System.out.println("Age: "); age = io.nextInt();
				System.out.println("Income: "); income = io.nextInt();
				
				responder = new Responder(new LoginData(username, password), new PersonalDetails(gender, age, income));
			} catch(Exception e) {
				System.out.println("Invalid input detected. Please start over.");
				invalid = true;
			}
		} while(invalid);
		//io.close();
	}
	
	private boolean checkGender(String gender) {
		if(gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("female"))
			return true;
		System.out.println("Gender must be stated as 'male' or 'female'");
		return false;
	}
	
	@Override
	public Customer getCustomer() {
		return responder;
	}

	
}