package com.dating;

import java.util.Scanner;

import com.dating.customer.CustomerHandler;
import com.dating.customer.MatchFinder;
import com.dating.customer.advert.Advertiser;
import com.dating.customer.respond.Responder;
import com.dating.io.AdvertiserRegistration;
import com.dating.io.ResponderRegistration;

public class Application {
	
	public Application() {
		// ' The customer handler holds all customers, the match finder uses it to search for compatibility
		MatchFinder matchFinder = new MatchFinder(new CustomerHandler());
		matchFinder.showNewMessages(); 
		
		// From here on, the whole program is based on ArrayList manipulation (which is nice)
		CustomerHandler list = matchFinder.getList();
		list.checkAdvertAccounts(); // logs in to each advert account and checks their messages

		// A simple scanner util used to add customers
		Scanner io = new Scanner(System.in);
		System.out.println("Please register a new customer.");
		boolean addAnother;
		do {
			addAnother = false;
			
			try {	
				boolean invalid;
				do {
					invalid = false;
					System.out.println("What kind of customer would you like to register? Type A for an advertiser, and R for a responder.");
					String type = io.nextLine();
					if(type.equalsIgnoreCase("A"))
						list.addAdvert((Advertiser)new AdvertiserRegistration().getCustomer());
					else if(type.equalsIgnoreCase("R"))
						list.addResponder((Responder) new ResponderRegistration().getCustomer());
					else {
						System.out.println("Invalid input. Please try again.");
						invalid = true;
					}
				} while(invalid);
				
				System.out.println("Would you like to add another customer? (Y/N)");
				String answer = io.nextLine();
				boolean validCustomerBool;
				do {
					validCustomerBool = false;
					if(answer.equalsIgnoreCase("Y"))
						addAnother = true;
					else if(answer.equalsIgnoreCase("N"))
						addAnother = false;
					else {
						System.out.println("Invalid input. Please try again.");
						validCustomerBool = true;
					}
				} while(validCustomerBool);
			} catch(Exception e) {
				System.out.println("Invalid input. Please try again.");
				addAnother = true;
			}
		} while(addAnother);
		
		System.out.println("Please remove a customer account. For a list of all account usernames, type '/list'");
		boolean invalid;
		do {
			invalid = false;
			System.out.println("Type a username to delete an account:");
			String input = io.nextLine();
			if(input.equalsIgnoreCase("/list")) {
				list.logUsernames();
				invalid = true;
			} else {
				list.removeAccount(input);
			}
		} while(invalid);
		io.close();
		
		System.out.println("UPDATED USER LIST:");
		list.logUsernames();
	}
}
